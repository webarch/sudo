# Copyright 2019-2024 Chris Croome
#
# This file is part of the Webarchitects Sudo Ansible role.
#
# The Webarchitects Sudo Ansible role is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# The Webarchitects Sudo Ansible role is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the Webarchitects Sudo Ansible role. If not, see <https://www.gnu.org/licenses/>.
---
- name: Verify as root
  # become: true
  connection: local
  # gather_facts: true
  hosts:
    - localhost
  vars:
    ansible_python_interpreter: /usr/bin/python3
  tasks:

    - name: Slurp the /etc/sudoers file
      ansible.builtin.slurp:
        path: /etc/sudoers
      register: molecule_sudoers_b64encoded

    - name: Decode the base64 encoded version of the suders file and set a variable
      ansible.builtin.set_fact:
        molecule_sudoers: "{{ molecule_sudoers_b64encoded['content'] | ansible.builtin.b64decode | ansible.builtin.split('\n') | reject('regex', '^#') | reject('regex', '^$') }}"

    - name: Debug molecule_sudoers
      ansible.builtin.debug:
        var: molecule_sudoers

    - name: Check that /etc/sudoers contains the expected content  # noqa: no-tabs
      ansible.builtin.assert:
        that:
          - molecule_sudoers == molecule_sudoers_required
      vars:
        molecule_sudoers_required:
          - "Defaults\tenv_reset"
          - "Defaults\tmail_badpass"
          - "Defaults\tsecure_path=\"/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin\""
          - "Defaults\tuse_pty"
          - "root\tALL=(ALL:ALL) ALL"
          - "%sudo\tALL=(ALL:ALL) NOPASSWD:ALL"
          - '@includedir /etc/sudoers.d'
...
