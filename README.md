# Webarchitects Ansible sudo role

Ansible role to install `sudo` and:

* By default, grant password-less `sudo` to people in the `sudo` group.
* Optionally grant specific users password-less `sudo`.
* Optionally grant specific groups password-less `sudo`.
* Optionally grant specific users password-less sudo to the `www-data` user account, for example `sudo -i -u www-data -s /bin/bash`.
* Optionally remove lines from `/etc/sudoers`.
* Allow the `nagios` user, if present, to run `/usr/bin/mailq`, if present.

See the [defaults file](defaults/main.yml) for options.
